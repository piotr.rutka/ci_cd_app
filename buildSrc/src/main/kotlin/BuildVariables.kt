import kotlin.reflect.KProperty0

fun getEnv(name: String) = System.getenv(name)?.let { if (it.isBlank()) null else it }

const val DEFAULT_APPLICATION_ID = "pl.rutka.sampleciapp"

object BuildVariables {

    val applicationId get() = getEnv("APPLICATION_ID") ?: DEFAULT_APPLICATION_ID

    //sdk
    val minSdkVersion get() = 21
    val targetSdkVersion get() = 30
    val compileSdkVersion get() = targetSdkVersion

    //version
    val versionCode get() = getEnv("BUILD_COUNTER")?.toIntOrNull() ?: 1             //BUILD_COUNTER is set at job
    val vcsVersion get() = getEnv("VCS_VERSION") ?: "development"                //VCS_VERSION is set at job

    private val versionString get() = "1.0.$versionCode"
    private val versionNameSuffix
        get() = if (vcsVersion.startsWith("dev")) vcsVersion.substring(0, 3)
        else vcsVersion.substring(0, 4)
    val versionName get() = "$versionString.${versionNameSuffix}"

    //keystore
    val keystoreAlias get() =
        if (applicationId == DEFAULT_APPLICATION_ID) "prod"
        else "staging"
    val keystorePassword get() = getEnv("KEYSTORE_PASSWORD")                     //KEYSTORE_PASSWORD is set globally at CI/CD variables level
    val keystoreKeyPassword
        get() = if (keystoreAlias == "prod") getEnv("KEYSTORE_KEY_PASSWORD_PROD")        //KEYSTORE_KEY_PASSWORD_PROD is set globally at CI/CD variables level
        else getEnv("KEYSTORE_KEY_PASSWORD_STAGING")                                     //KEYSTORE_KEY_PASSWORD_STAGING is set globally at CI/CD variables level


    //create BuildConfig fields
    fun toBuildConfigFields() = listOf<KProperty0<Any>>(
        BuildVariables::minSdkVersion,
        BuildVariables::targetSdkVersion,
        BuildVariables::compileSdkVersion,
        BuildVariables::versionCode,
        BuildVariables::versionName,
        BuildVariables::vcsVersion,
        BuildVariables::applicationId
    ).map {
        val value = it.get()
        val (type, literal) = when (value) {
            is String -> "String" to "\"$value\""
            else -> value.javaClass.name to "$value"
        }
        BuildConfigField(it.name, type, literal)
    }
}

data class BuildConfigField(val name: String, val type: String, val value: String)