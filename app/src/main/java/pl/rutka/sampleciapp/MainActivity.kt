package pl.rutka.sampleciapp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val stringBuffer = StringBuffer().apply {
            append(BuildConfig.APPLICATION_ID)
            append("\n")
            append(BuildConfig.VERSION_CODE)
            append("\n")
            append(BuildConfig.versionName)
            append("\n")
        }

        helloWorld.text = stringBuffer.toString()
    }
}